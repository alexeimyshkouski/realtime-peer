import Peer from './peer'

export default class BrowserPeer extends Peer {
    listen() {
        throw new Error(`WebRTC are not supported yet`)
    }
}