const DEFAULT_WS_CLOSE_CODE = 1008
const DEFAULT_UPGRADE_STATUS_CODE = 101

const ERROR_EVENT = 'error'

export async function upgrade(request, socket, head, extensions, headers) {
    try {
        const ctx = this.createConnectionContext({
            app: this,
            request,
            socket,
            head,
            extensions,
            statusCode: DEFAULT_UPGRADE_STATUS_CODE
        })

        await this._onConnectOffer.push(ctx)

        this._server.completeUpgrade(request, socket, head, extensions, headers)
    } catch (error) {
        this.emit(ERROR_EVENT, error)
    }
}

export function connection(websocket, req, socket, head, extensions, headers) {
    websocket
        .on('message', message.bind(this, websocket, req, socket, head, extensions, headers))
        .once(ERROR_EVENT, error.bind(this, websocket))
}

async function message(websocket, request, socket, head, extensions, headers, message) {
    try {
        const ctx = this.createIncomingContext({
            app: this,
            message,
            websocket,
            request,
            socket,
            statusCode: DEFAULT_WS_CLOSE_CODE
        })

        await this._onIncomingMessage.push(ctx)
    } catch (error) {
        this.emit('error', error)
    }
}

function error(websocket, error) {
    websocket.terminate()
    this._errorHandler(error)
}