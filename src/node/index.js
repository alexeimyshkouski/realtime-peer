import { Server } from '@alexeimyshkouski/ws'
import { UpgradeContext, ClientMessageContext } from '@alexeimyshkouski/realtime-core/context'

import Peer from '../peer'
import * as handlers from './handlers'

class NodePeer extends Peer {
    static ConnectOfferContext = UpgradeContext
    static IncomingContext = ClientMessageContext

    constructor(options = {}) {
        super()

        this._server = new Server(Object.assign({}, options, { noServer: true }))
        this._server
            .on('upgrade', handlers.upgrade.bind(this))
            .on('connection', handlers.connection.bind(this))
    }

    get connected() {
        return this._server.clients
    }

    upgrade(fn) {
        // if (this._onIncomingMessage.length) {
        //     throw new Error('Upgrade middleware should be used before message middlewares')
        // }

        return this.connect(fn)
    }

    callback() {
        return this._server.handleUpgrade.bind(this._server)
    }

    listen(...args) {
        const server = require('http').createServer()

        server.on('upgrade', this.callback())

        return server.listen(...args)
    }
}

export default NodePeer