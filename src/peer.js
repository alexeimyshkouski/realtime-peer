import Node from '@alexeimyshkouski/realtime-core/node'
import { service, publisher, subscriber } from '@alexeimyshkouski/realtime-core/roles'

@service @publisher @subscriber
class Peer extends Node { }

export default Peer