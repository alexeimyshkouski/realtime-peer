import * as paths from './paths'

const formats = [
    ['cjs', {
        dir: paths.dist,
        entryFileNames: '[name].js',
        chunkFileNames: 'chunks/[hash].js',
        sourcemap: true
    }],
    ['esm', {
        dir: paths.dist,
        entryFileNames: '[name].mjs',
        chunkFileNames: 'chunks/[hash].mjs',
        preferConst: true,
        sourcemap: true
    }],
    ['iife', {
        dir: paths.dist,
        entryFileNames: '[name].js',
        name: 'RealtimePeer',
        sourcemap: true
    }]
].map((entry) => {
    entry[1].format = entry[0]
    return entry
})

export default new Map(formats)