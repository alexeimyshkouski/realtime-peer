import babel from 'rollup-plugin-babel'

const commonPlugins = [
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    '@babel/plugin-proposal-class-properties'
]

const commonOptions = {
    babelrc: false,
    runtimeHelpers: true,
    exclude: 'node_modules/**'
}

const browserSpecificOptions = {
    presets: [
        ['@babel/preset-env', {
            targets: ['> 1%', 'not dead']
        }]
    ],
    plugins: [
        ...commonPlugins,
        ['@babel/plugin-transform-runtime', {
            absoluteRuntime: false,
            corejs: false,
            helpers: true,
            regenerator: false,
            useESModules: true
        }]
    ]
}

const nodeSpecificOptions = {
    presets: [
        ['@babel/preset-env', {
            targets: ['maintained node versions']
        }]
    ],
    plugins: [
        ...commonPlugins,
        '@babel/plugin-transform-runtime'
    ]
}

const browserOptions = Object.assign({}, commonOptions, browserSpecificOptions)
const nodeOptions = Object.assign({}, commonOptions, nodeSpecificOptions)

export const browser = () => babel(browserOptions)
export const node = () => babel(nodeOptions)
