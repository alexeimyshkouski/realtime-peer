import path from 'path'

export const app = path.resolve(__dirname, '..')
export const src = path.join(app, 'src')
export const dist = path.join(app, 'dist')