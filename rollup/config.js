
import path from 'path'

import commonjs from 'rollup-plugin-commonjs'
import { terser } from 'rollup-plugin-terser'

import * as babel from './babel'
import * as paths from './paths'
import resolve from './resolve'
import formats from './formats'
import external from './external'

export default [
    {
        input: {
            'node': path.join(paths.src, 'node/index.js')
        },
        output: [
            formats.get('esm'),
            formats.get('cjs')
        ],
        treeshake: {
            moduleSideEffects: 'no-external'
        },
        external,
        plugins: [
            babel.node(),
            commonjs(),
            resolve('node')
        ]
    },

    {
        input: {
            'browser': path.join(paths.src, 'browser.js')
        },
        output: formats.get('esm'),
        treeshake: {
            moduleSideEffects: 'no-external'
        },
        external,
        plugins: [
            babel.browser(),
            commonjs(),
            resolve('browser')
        ]
    },

    {
        input: {
            'browser': path.join(paths.src, 'browser.js')
        },
        output: formats.get('iife'),
        plugins: [
            babel.browser(),
            commonjs(),
            resolve('browser'),
            terser()
        ]
    }
]