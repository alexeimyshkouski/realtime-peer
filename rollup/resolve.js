import resolve from 'rollup-plugin-node-resolve'

const extensions = ['.mjs', '.js', '.json']

export default isBrowser => {
    const options = isBrowser === 'browser' ? {
        preferBuiltins: false,
        mainFields: ['module', 'browser', 'main'],
        extensions
    } : {
        preferBuiltins: true,
        mainFields: ['module', 'main'],
        extensions
    }

    return resolve(options)
}