export default (id, parentId, isResolved) => {
    if (parentId) {
        if (isResolved) {
            // console.log(/node_modules/.test(id), id)
            return /node_modules/.test(id)
        }

        // console.log(/^[^.]/.test(id), id)
        return /^[^.]/.test(id)
    }

    // console.log(false, id)
    return false
}