import _classCallCheck from '@babel/runtime/helpers/esm/classCallCheck';
import _createClass from '@babel/runtime/helpers/esm/createClass';
import _possibleConstructorReturn from '@babel/runtime/helpers/esm/possibleConstructorReturn';
import _getPrototypeOf from '@babel/runtime/helpers/esm/getPrototypeOf';
import _inherits from '@babel/runtime/helpers/esm/inherits';
import Node from '@alexeimyshkouski/realtime-core/node';
import { service, publisher, subscriber } from '@alexeimyshkouski/realtime-core/roles';

var _class;

var Peer = service(_class = publisher(_class = subscriber(_class =
/*#__PURE__*/
function (_Node) {
  _inherits(Peer, _Node);

  function Peer() {
    _classCallCheck(this, Peer);

    return _possibleConstructorReturn(this, _getPrototypeOf(Peer).apply(this, arguments));
  }

  return Peer;
}(Node)) || _class) || _class) || _class;

var BrowserPeer =
/*#__PURE__*/
function (_Peer) {
  _inherits(BrowserPeer, _Peer);

  function BrowserPeer() {
    _classCallCheck(this, BrowserPeer);

    return _possibleConstructorReturn(this, _getPrototypeOf(BrowserPeer).apply(this, arguments));
  }

  _createClass(BrowserPeer, [{
    key: "listen",
    value: function listen() {
      throw new Error("WebRTC are not supported yet");
    }
  }]);

  return BrowserPeer;
}(Peer);

export default BrowserPeer;
//# sourceMappingURL=browser.mjs.map
