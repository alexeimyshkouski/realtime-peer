import _defineProperty from '@babel/runtime/helpers/defineProperty';
import { Server } from '@alexeimyshkouski/ws';
import { UpgradeContext, ClientMessageContext } from '@alexeimyshkouski/realtime-core/context';
import Node from '@alexeimyshkouski/realtime-core/node';
import { service, publisher, subscriber } from '@alexeimyshkouski/realtime-core/roles';

var _class;

let Peer = service(_class = publisher(_class = subscriber(_class = class Peer extends Node {}) || _class) || _class) || _class;

const DEFAULT_WS_CLOSE_CODE = 1008;
const DEFAULT_UPGRADE_STATUS_CODE = 101;
const ERROR_EVENT = 'error';
async function upgrade(request, socket, head, extensions, headers) {
  try {
    const ctx = this.createConnectionContext({
      app: this,
      request,
      socket,
      head,
      extensions,
      statusCode: DEFAULT_UPGRADE_STATUS_CODE
    });
    await this._onConnectOffer.push(ctx);

    this._server.completeUpgrade(request, socket, head, extensions, headers);
  } catch (error) {
    this.emit(ERROR_EVENT, error);
  }
}
function connection(websocket, req, socket, head, extensions, headers) {
  websocket.on('message', message.bind(this, websocket, req, socket, head, extensions, headers)).once(ERROR_EVENT, error.bind(this, websocket));
}

async function message(websocket, request, socket, head, extensions, headers, message) {
  try {
    const ctx = this.createIncomingContext({
      app: this,
      message,
      websocket,
      request,
      socket,
      statusCode: DEFAULT_WS_CLOSE_CODE
    });
    await this._onIncomingMessage.push(ctx);
  } catch (error) {
    this.emit('error', error);
  }
}

function error(websocket, error) {
  websocket.terminate();

  this._errorHandler(error);
}

class NodePeer extends Peer {
  constructor(options = {}) {
    super();
    this._server = new Server(Object.assign({}, options, {
      noServer: true
    }));

    this._server.on('upgrade', upgrade.bind(this)).on('connection', connection.bind(this));
  }

  get connected() {
    return this._server.clients;
  }

  upgrade(fn) {
    // if (this._onIncomingMessage.length) {
    //     throw new Error('Upgrade middleware should be used before message middlewares')
    // }
    return this.connect(fn);
  }

  callback() {
    return this._server.handleUpgrade.bind(this._server);
  }

  listen(...args) {
    const server = require('http').createServer();

    server.on('upgrade', this.callback());
    return server.listen(...args);
  }

}

_defineProperty(NodePeer, "ConnectOfferContext", UpgradeContext);

_defineProperty(NodePeer, "IncomingContext", ClientMessageContext);

export default NodePeer;
//# sourceMappingURL=node.mjs.map
